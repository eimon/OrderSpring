package orderexercise;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {

	@Autowired
	private ProductDAL productDal;
	List<Product> productList;
	

	@RequestMapping(value = "/")
	public String index() {
		// return new ModelAndView("redirect:/product");
		return "redirect:product";
	}

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public ModelAndView product(@RequestParam(required = false) Integer page) {
		
		//int pageNo = Integer.parseInt(request.getParameter("page"));
		
		
		try {
			productList = productDal.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		PagedListHolder<Product> pagedListHolder = new PagedListHolder<>(productList);
	       pagedListHolder.setPageSize(5);        
	       ModelAndView modelView = new ModelAndView("product");
	       modelView.addObject("maxPages", pagedListHolder.getPageCount());
	       if (page == null || page < 1 || page > pagedListHolder.getPageCount())
	           page = 1;        
	       modelView.addObject("page", page);
/*	       if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
	           pagedListHolder.setPage(0);
	           modelView.addObject("productList", pagedListHolder.getPageList());
	       } else */
	    	   if (page <= pagedListHolder.getPageCount()) {
	           pagedListHolder.setPage(page - 1);
	           modelView.addObject("productList", pagedListHolder.getPageList());
	       }
	    	   
		return modelView;
	}

	@RequestMapping(value = "/addproduct", method = RequestMethod.GET)
	public ModelAndView addProduct() {

		ModelAndView modelView = new ModelAndView("addproduct", "product", new Product());
		return modelView;
	}

	@RequestMapping(value = "/addproduct", method = RequestMethod.POST)
	public ModelAndView addproduct(@ModelAttribute("product") @Valid Product product, BindingResult result, ModelMap model) {
		
		if(result.hasErrors()) {			
			return new ModelAndView("addproduct");
        }
		
		try {
			productDal.add(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/editproduct", method = RequestMethod.GET)
	public ModelAndView editProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {

		int id = Integer.parseInt(request.getParameter("id"));

		Product product = findProductById(id);

		ModelAndView modelView = new ModelAndView("editproduct", "command", product);
		return modelView;
	}

	@RequestMapping(value = "/editproduct", method = RequestMethod.POST)
	public ModelAndView updateproduct(@ModelAttribute("SpringWeb") Product product, ModelMap model) {

		try {
			productDal.update(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/deleteproduct", method = RequestMethod.GET)
	public ModelAndView deleteproduct(HttpServletRequest request, HttpServletResponse response) {

		// MainForm mainForm = new MainForm();
		// if(mainForm.showConfirmMessage("Are You Sure You Want to Delete.")){

		int id = Integer.parseInt(request.getParameter("id"));
		Product product = findProductById(id);

		try {
			productDal.delete(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		// }

		return new ModelAndView("redirect:/product");
	}

	/*
	 * @RequestMapping(value = "/download", method = RequestMethod.GET) public
	 * ModelAndView download(HttpServletRequest request, HttpServletResponse
	 * response) {
	 * 
	 * String csvFileName = "product.csv"; response.setContentType("text/csv");
	 * String headerKey = "Content-Disposition"; String headerValue =
	 * String.format("attachment; filename=\"%s\"", csvFileName);
	 * response.setHeader(headerKey, headerValue);
	 * 
	 * generateCsvFile("E:\\products.csv");
	 * 
	 * return new ModelAndView("redirect:/product"); }
	 */

	@RequestMapping(value = "/downloadCSV")
	public void downloadCSV(HttpServletResponse response) throws IOException {
		String csvFileName = "product.csv";
		response.setContentType("text/csv");
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);

		ArrayList<String> rows = new ArrayList<String>();

		for (Product product : productList) {
			rows.add((String.valueOf(product.getId())));
			rows.add(",");
			rows.add(product.getName());
			rows.add(",");
			rows.add(String.valueOf(product.getPrice()));
			rows.add("\n");

		}
		Iterator<String> iter = rows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();

			response.getOutputStream().print(outputString);

		}
		response.getOutputStream().flush();
	}
	
	@RequestMapping(value="/list")
	 public String list(Model model, Integer offset, Integer maxResults){
	  model.addAttribute("persons", productDal.list(offset, maxResults));
	  model.addAttribute("count", productDal.count());
	  model.addAttribute("offset", offset);
	  return "redirect:list";
	 }

	private Product findProductById(int id) {

		Product product = new Product();

		for (Product p : productList) {
			if (p.getId() == id) {
				product.setId(p.getId());
				product.setName(p.getName());
				product.setPrice(p.getPrice());
			}
		}

		return product;
	}

	/*
	 * private void generateCsvFile(String sFileName) {
	 * 
	 * try { FileWriter writer = new FileWriter(sFileName);
	 * 
	 * writer.append("Id"); writer.append(','); writer.append("Name");
	 * writer.append(','); writer.append("Price"); writer.append('\n');
	 * 
	 * for (Product product : productList) {
	 * writer.append(String.valueOf(product.getId())); writer.append(',');
	 * writer.append(product.getName()); writer.append(',');
	 * writer.append(String.valueOf(product.getPrice())); writer.append('\n'); }
	 * 
	 * // generate whatever data you want
	 * 
	 * writer.flush(); writer.close(); } catch (IOException e) {
	 * e.printStackTrace(); } }
	 */
}
