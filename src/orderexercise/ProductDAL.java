/**
 * @author myatmyolwin
 */

package orderexercise;


import java.sql.SQLException;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


public class ProductDAL {
	
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public List<Product> getAll() throws SQLException, ClassNotFoundException {
		
		org.hibernate.Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Product");
		List<Product> products = query.list();
		return products;

	}

	public void add(Product product) throws SQLException, ClassNotFoundException {

		org.hibernate.Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(product);
		session.getTransaction().commit();
		session.close();
		
	}

	public void update(Product product) throws SQLException, ClassNotFoundException {

		org.hibernate.Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(product);
		session.getTransaction().commit();
		session.close();

	}

	public void delete(Product product) throws SQLException, ClassNotFoundException {

		org.hibernate.Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(product);
		session.getTransaction().commit();
		session.close();

	}
	
	@SuppressWarnings("unchecked")
	 @Transactional
	 public List<Product> list(Integer offset, Integer maxResults){
	  return sessionFactory.openSession()
	    .createCriteria(Product.class)
	    .setFirstResult(offset!=null?offset:0)
	    .setMaxResults(maxResults!=null?maxResults:10)
	    .list();
	 }
	  
	  
	 public Long count(){
	  return (Long)sessionFactory.openSession()
	    .createCriteria(Product.class)
	    .setProjection(Projections.rowCount())
	    .uniqueResult();
	 }
	  /*
	  
	 public void save(){
	  for(int itr=1;itr <= 100 ; itr++){
		  Product product = new Product("Product_"+itr,Math.max(25, (itr%2)*35));
	   sessionFactory.openSession()
	   .save(product);
	  }*/

}
