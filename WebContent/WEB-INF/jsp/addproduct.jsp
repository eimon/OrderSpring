<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet"
   href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">    
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<title>Add Product</title>
<!-- <style>
	body{
		background-image: url("images/cd.jpg");
		background-color:  #FB667A;
	}
</style> -->
</head>
<body>
<br>

 <div class="container-fluid">
<h2 class="bg-danger">Add Product</h2>
</div>

<br><br>
 <div class="container-fluid">
	<form:form method="POST" action="/OrderSpring/addproduct" commandName="product">
	<div class="form-group">
	<form:label for="exampleInputName" path="name"> Name</form:label>
    <form:input type="name" class="form-control" id="exampleInputName" placeholder="Name" path="name" />
    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
  	<span id="inputError2Status" class="sr-only">(error)</span>
    <form:errors path="name"></form:errors>
    </div>
    <div class="form-group">
    <form:label for="exampleInputPrice" path="price">Price</form:label>
    <form:input type="price" class="form-control" id="exampleInputPrice" placeholder="Price" path="price" />
    <form:errors path="price"></form:errors>
    </div>
    <dib align="center">
    <button type="submit" class="btn btn-primary" align="center"/>Save</td>    
    </div>
	</form:form>
</div>	
</body>
</html>