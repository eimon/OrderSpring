<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <link href="<c:url value='resources/css/style.css'/>" rel="stylesheet" type="text/css"/>
<%-- <script src="<c:url value="/resources/js/main.js" />"></script> --%>
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet"
   href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">    
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<title>Albums Available</title>
</head>
<body>

<ul>
  <li><a class="active" href="#home">Home</a></li>
  <li><a href="http://localhost:8080/OrderSpring/addproduct">New</a></li>
  <li><a href="http://localhost:8080/OrderSpring/downloadCSV">Download CSV</a></li>
  <li><a href="#about">About</a></li>
</ul>
<h1>Albums</h1>

<table border="1" width="500" cellspacing="0" class="container" align="left">
<!-- inside loop create rows  -->
<tr>
    <th></th>
    <th>Name</th>
    <th>Price</th>
    <th colspan=2>Actions</th>
  </tr>
	<c:forEach var="product" items="${productList}" >
		<tr align="center">
			<td>${product.id}</td>
			<td>${product.name}</td>
			<td>${product.price}</td>
			<td><a href="http://localhost:8080/OrderSpring/editproduct?id=${product.id}" class="strange">Edit</a></td>
			<td><a href="http://localhost:8080/OrderSpring/deleteproduct?id=${product.id}" onClick="return confirm('Are you sure want to delete?')" class="strange">Delete</a></td>
		</tr>
	</c:forEach>

<!-- end inside loop create rows  -->
</table>

<%-- <c:forEach var="page" items="${pages}">
<a href="http://localhost:8080/OrderSpring/product?page=${page}">${page}</a>
</c:forEach> --%>
<div class="container">
<ul class="pagination">
       <c:url value="/product" var="prev">
           <c:param name="page" value="${page-1}" />
       </c:url>
       <c:if test="${page > 1}">
          <li> <a href="<c:out value="${prev}" />" class="pn prev">Prev</a></li>
       </c:if>        
       <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
           <c:choose>
               <c:when test="${page == i.index}">
                   <li class="active"><span>${i.index}</span></li>
               </c:when>
               <c:otherwise>
                   <c:url value="/product" var="url">
                       <c:param name="page" value="${i.index}" />
                   </c:url>
                  <li> <a href='<c:out value="${url}" />'>${i.index}</a></li>
               </c:otherwise>
           </c:choose>
       </c:forEach>
       <c:url value="/product" var="next">
           <c:param name="page" value="${page + 1}" />
       </c:url>
       <c:if test="${page + 1 <= maxPages}">
           <li><a href='<c:out value="${next}" />' class="pn next">Next</a></li>
       </c:if>
       </ul>
   </div>
</body>
</html>