<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
 <link href="<c:url value='resources/css/style2.css'/>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet"
   href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">    
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<title>Edit Product</title>
</head>
<body>
<div class="container-fluid">
<h2 class="bg-danger">Edit Product</h2>
</div>

<br><br>
 <div class="container-fluid">
 
	<form:form method="POST" action="/OrderSpring/editproduct">	
	<div class="form-group">
	<form:label path="name"> Name</form:label>
	<form:input class="form-control" path="name" />
	</div>
    <div class="form-group">
    <form:label path="price">Price</form:label>
	<form:input class="form-control" path="price" />
	<form:hidden path="id" /></td>
	</div>
	<button type="submit" class="btn btn-primary" align="center"/>Edit</td> 	
	</form:form>
</div>	
</body>
</html>